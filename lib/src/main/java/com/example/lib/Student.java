package com.example.lib;

public class Student extends Human {
    public Integer getStudentId() {
        return studentId;
    }
    public void setStudentId(Integer studentId) {
        this.studentId = studentId;
    }

    private  Integer studentId;
    public Student(String firstName,String lastName,Integer age,String address,Integer studentId)
    {
        setFirstName(firstName);
        setLastName(lastName);
        setAddress(address);
        setAge(age);
        setStudentId(studentId);
    }
    public  Student(){}
    public String FillStudentSpec()
    {
        Integer number=1;
        String result= String.format("Student %d :%s %s  with Student ID :%d is %d,Address:%s ",
                number,getFirstName(),getLastName(),getStudentId(),getAge(),getAddress());
        return result;
    }



}
