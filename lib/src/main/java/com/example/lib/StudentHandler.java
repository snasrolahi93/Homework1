package com.example.lib;

import java.util.ArrayList;
import java.util.List;

import Utils.Util;

public class StudentHandler {
    public List<Student> students=new ArrayList<>();
    public void FillStudentList()
    {
        Student s1=new Student();
        s1.setFirstName("Masoud");
        s1.setLastName("Daneshpour");
        s1.setFullName(s1.getFirstName(),s1.getLastName());
        s1.setAddress("Soheilain Street,Number 233");
        s1.setAge(30);
        s1.setStudentId(9701);
        students.add(s1);
        Student s2=new Student("Zahra","Karimi",29,"khabazi Street,number 2",9702);
        s2.setFullName(s2.getFirstName(),s2.getLastName());
        students.add(s2);
        Student s3=new Student("AliReza","Najib",32,"khabazi Street,number 3",9703);
        s3.setFullName(s3.getFirstName(),s3.getLastName());
        students.add(s3);
    }

    public void iterateStudents()
    {

        for (Student student:students) {
           student.FillStudentSpec();


        }
    }
    public void  ShowStudentList()
    {
        for (Student student:students) {
           String result= student.FillStudentSpec();
            Util.printString(result);

        }
    }

}
