package com.example.lib;

public class Author {
    public String getWriter() {
        return writer;
    }

    public void setWriter(String writer) {
        this.writer = writer;
    }

    private String writer;
    public Author(String writer)
    {
        setWriter(writer);
    }
}
